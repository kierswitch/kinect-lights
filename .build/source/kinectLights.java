import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import SimpleOpenNI.*; 
import dmxP512.*; 
import processing.serial.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class kinectLights extends PApplet {

// This will be converted to OpenFrameworks later for speed and flexability reasons
// SimpleOpenNI: https://github.com/totovr/SimpleOpenni
// SimpleOpenNI install instructions: https://github.com/totovr/SimpleOpenNI/blob/master/instructions.md
// DmxP512 instructions: http://motscousus.com/stuff/2011-01_dmxP512/

// Kinect broken? /libraries/libfreenect2/build and run `make install`
// Or on a new computer remove existing build directory and run `mkdir build; cd build; cmake; make; make install`





//Generate a SimpleOpenNI object
SimpleOpenNI kinect;

DmxP512 dmxOutput;
int universeSize=128;

String DMXPRO_PORT="/dev/cu.usbserial-6A301V80"; // From `ls /dev`
int DMXPRO_BAUDRATE=115000;

Serial myPort;  // Create object from Serial class

public void setup() {
        kinect = new SimpleOpenNI(this);
        int trys = 0;
        while(kinect.deviceCount() < 1) {
            if(trys > 5) {
                exit();
                return;
            }
            System.out.format("Error: Kinect not found! Trying again in 3 seconds... (Try %d out of 5)", trys);
            delay(3000);
            kinect = new SimpleOpenNI(this);
            trys++;
        }
        kinect.enableDepth();
        kinect.enableUser();// because of the version this change
        
        fill(255, 0, 0);
        kinect.setMirror(true);

        dmxOutput=new DmxP512(this,universeSize,false);
        dmxOutput.setupDmxPro(DMXPRO_PORT,DMXPRO_BAUDRATE);

        //Open the serial port
        //String portName = Serial.list()[1]; //change the 0 to a 1 or 2 etc. to match your port
        //myPort = new Serial(this, portName, 9600);

}

public void draw() {
        kinect.update();
        image(kinect.depthImage(), 0, 0);

        IntVector userList = new IntVector();
        kinect.getUsers(userList);

        if (userList.size() > 0) {
                int userId = userList.get(0);
                //If we detect one user we have to draw it
                if ( kinect.isTrackingSkeleton(userId)) {
                        //User connected
                        //onNewUser(kinect, userId);
                        //Draw the skeleton user
                        drawSkeleton(userId);
                        // get the positions of the three joints of our arm
                        PVector rightHand = new PVector();
                        kinect.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_HAND,rightHand);
                        PVector rightElbow = new PVector();
                        kinect.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_ELBOW,rightElbow);
                        PVector rightShoulder = new PVector();
                        kinect.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_SHOULDER,rightShoulder);
                        // we need right hip to orient the shoulder angle
                        PVector rightHip = new PVector();
                        kinect.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_HIP,rightHip);
                        // reduce our joint vectors to two dimensions
                        PVector rightHand2D = new PVector(rightHand.x, rightHand.y);
                        PVector rightElbow2D = new PVector(rightElbow.x, rightElbow.y);
                        PVector rightShoulder2D = new PVector(rightShoulder.x,rightShoulder.y);
                        PVector rightHip2D = new PVector(rightHip.x, rightHip.y);
                        // calculate the axes against which we want to measure our angles
                        PVector torsoOrientation = PVector.sub(rightShoulder2D, rightHip2D);
                        PVector upperArmOrientation = PVector.sub(rightElbow2D, rightShoulder2D);
                        // calculate the angles between our joints
                        float shoulderAngle = angleOf(rightElbow2D, rightShoulder2D, torsoOrientation);
                        float elbowAngle = angleOf(rightHand2D,rightElbow2D,upperArmOrientation);
                        // show the angles on the screen for debugging
                        fill(255,0,0);
                        scale(3);
                        text("r hand x: " + PApplet.parseInt(rightHand.x) + "\n" + " r hand y: " + PApplet.parseInt(rightHand.y), 20, 20);
                }
        }
}

public void drawSkeleton(int userId) {
        stroke(0);
        strokeWeight(5);
        kinect.drawLimb(userId, SimpleOpenNI.SKEL_HEAD, SimpleOpenNI.SKEL_NECK);
        kinect.drawLimb(userId, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_LEFT_SHOULDER);
        kinect.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_LEFT_ELBOW);
        kinect.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_ELBOW, SimpleOpenNI.SKEL_LEFT_HAND);
        kinect.drawLimb(userId, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_RIGHT_SHOULDER);
        kinect.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_RIGHT_ELBOW);
        kinect.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_ELBOW, SimpleOpenNI.SKEL_RIGHT_HAND);
        kinect.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_TORSO);
        kinect.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_TORSO);
        kinect.drawLimb(userId, SimpleOpenNI.SKEL_TORSO, SimpleOpenNI.SKEL_LEFT_HIP);
        kinect.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_HIP, SimpleOpenNI.SKEL_LEFT_KNEE);
        kinect.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_KNEE, SimpleOpenNI.SKEL_LEFT_FOOT);
        kinect.drawLimb(userId, SimpleOpenNI.SKEL_TORSO, SimpleOpenNI.SKEL_RIGHT_HIP);
        kinect.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_HIP, SimpleOpenNI.SKEL_RIGHT_KNEE);
        kinect.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_KNEE, SimpleOpenNI.SKEL_RIGHT_FOOT);
        kinect.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_HIP, SimpleOpenNI.SKEL_LEFT_HIP);
        noStroke();
        fill(255,0,0);
        drawJoint(userId, SimpleOpenNI.SKEL_HEAD);
        drawJoint(userId, SimpleOpenNI.SKEL_NECK);
        drawJoint(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER);
        drawJoint(userId, SimpleOpenNI.SKEL_LEFT_ELBOW);
        drawJoint(userId, SimpleOpenNI.SKEL_NECK);
        drawJoint(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER);
        drawJoint(userId, SimpleOpenNI.SKEL_RIGHT_ELBOW);
        drawJoint(userId, SimpleOpenNI.SKEL_TORSO);
        drawJoint(userId, SimpleOpenNI.SKEL_LEFT_HIP);
        drawJoint(userId, SimpleOpenNI.SKEL_LEFT_KNEE);
        drawJoint(userId, SimpleOpenNI.SKEL_RIGHT_HIP);
        drawJoint(userId, SimpleOpenNI.SKEL_LEFT_FOOT);
        drawJoint(userId, SimpleOpenNI.SKEL_RIGHT_KNEE);
        drawJoint(userId, SimpleOpenNI.SKEL_LEFT_HIP);
        drawJoint(userId, SimpleOpenNI.SKEL_RIGHT_FOOT);
        drawJoint(userId, SimpleOpenNI.SKEL_RIGHT_HAND);
        drawJoint(userId, SimpleOpenNI.SKEL_LEFT_HAND);
}

public void drawJoint(int userId, int jointId) {
        PVector joint = new PVector();
        float confidence = kinect.getJointPositionSkeleton(userId, jointId, joint);
        if(confidence < 0.9f) {
                return;
        }
        PVector convertedJoint = new PVector();
        kinect.convertRealWorldToProjective(joint, convertedJoint);
        ellipse(convertedJoint.x, convertedJoint.y, 5, 5);
}
//Generate the angle
public float angleOf(PVector one, PVector two, PVector axis) {
        PVector limb = PVector.sub(two, one);
        return degrees(PVector.angleBetween(limb, axis));
}
//Calibration not required
public void onNewUser(SimpleOpenNI kinect, int userId) {
        println("Start skeleton tracking");
        kinect.startTrackingSkeleton(userId);
}
  public void settings() {  size(640, 480); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "kinectLights" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
